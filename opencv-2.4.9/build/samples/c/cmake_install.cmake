# Install script for directory: /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xsamplesx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/OpenCV/samples/c" TYPE FILE PERMISSIONS OWNER_READ GROUP_READ WORLD_READ FILES
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/JCB.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/adaptiveskindetector.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/agaricus-lepiota.data"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/airplane.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/baboon.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/baboon200.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/baboon200_rotated.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/bgfg_codebook.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/blobtrack_sample.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/box.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/box_in_scene.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/build_all.sh"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/cat.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/contours.c"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/convert_cascade.c"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/cvsample.dsp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/delaunay.c"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/facedetect.cmd"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/facedetect.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/fback_c.c"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/find_obj.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/find_obj_calonder.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/find_obj_ferns.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/fruits.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/latentsvmdetect.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/lena.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/morphology.c"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/motempl.c"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/mser_sample.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/mushroom.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/one_way_sample.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/one_way_train_0000.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/one_way_train_0001.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/polar_transforms.c"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/puzzle.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/pyramid_segmentation.c"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/smiledetect.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/stuff.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/tree_engine.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/c/waveform.data"
    )
endif()

