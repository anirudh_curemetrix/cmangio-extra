# Install script for directory: /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xsamplesx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/OpenCV/samples/gpu/performance" TYPE FILE PERMISSIONS OWNER_READ GROUP_READ WORLD_READ FILES
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/performance/performance.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/performance/performance.h"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/performance/tests.cpp"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xsamplesx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/OpenCV/samples/gpu" TYPE FILE PERMISSIONS OWNER_READ GROUP_READ WORLD_READ FILES
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/aloeL.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/aloeR.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/alpha_comp.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/basketball1.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/basketball2.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/bgfg_segm.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/brox_optical_flow.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/cascadeclassifier.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/cascadeclassifier_nvidia_api.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/driver_api_multi.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/driver_api_stereo_multi.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/farneback_optical_flow.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/generalized_hough.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/hog.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/houghlines.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/morphology.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/multi.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/opengl.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/optical_flow.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/opticalflow_nvidia_api.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/pyrlk_optical_flow.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/road.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/rubberwhale1.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/rubberwhale2.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/stereo_match.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/stereo_multi.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/super_resolution.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/surf_keypoint_matcher.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/tsucuba_left.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/tsucuba_right.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/video_reader.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/gpu/video_writer.cpp"
    )
endif()

