# Install script for directory: /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xsamplesx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/OpenCV/samples/cpp" TYPE FILE PERMISSIONS OWNER_READ GROUP_READ WORLD_READ FILES
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/3calibration.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/OpenEXRimages_HDR_Retina_toneMapping.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/OpenEXRimages_HDR_Retina_toneMapping_video.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/baboon.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/bagofwords_classification.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/bgfg_gmg.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/bgfg_segm.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/board.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/brief_match_test.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/build3dmodel.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/building.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/calibration.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/calibration_artificial.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/camshiftdemo.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/chamfer.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/connected_components.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/contours2.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/convexhull.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/cout_mat.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/delaunay2.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/demhist.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/descriptor_extractor_matcher.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/detection_based_tracker_sample.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/detector_descriptor_evaluation.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/detector_descriptor_matcher_evaluation.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/dft.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/distrans.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/drawing.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/edge.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/em.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/fabmap_sample.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/facerec_demo.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/fback.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/ffilldemo.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/filestorage.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/fitellipse.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/freak_demo.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/fruits.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/gencolors.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/generic_descriptor_match.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/grabcut.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/houghcircles.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/houghlines.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/hybridtrackingsample.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/image.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/image_sequence.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/imagelist_creator.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/inpaint.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/intelperc_capture.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/kalman.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/kmeans.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/laplace.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/latentsvm_multidetect.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/left01.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/left02.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/left03.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/left04.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/left05.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/left06.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/left07.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/left08.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/left09.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/left11.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/left12.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/left13.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/left14.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/lena.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/letter-recognition.data"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/letter_recog.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/linemod.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/lkdemo.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/logo.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/logo_in_clutter.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/logpolar_bsm.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/matcher_simple.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/matching_to_many_images.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/meanshift_segmentation.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/minarea.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/morphology2.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/opencv_version.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/openni_capture.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/pca.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/peopledetect.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/phase_corr.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/pic1.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/pic2.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/pic3.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/pic4.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/pic5.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/pic6.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/points_classifier.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/retinaDemo.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/rgbdodometry.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/right01.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/right02.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/right03.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/right04.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/right05.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/right06.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/right07.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/right08.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/right09.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/right11.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/right12.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/right13.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/right14.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/segment_objects.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/select3dobj.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/simpleflow_demo.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/squares.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/starter_imagelist.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/starter_video.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/stereo_calib.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/stereo_match.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/stitching.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/stitching_detailed.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/stuff.jpg"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/templ.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/tsukuba_l.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/tsukuba_r.png"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/tvl1_optical_flow.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/video_dmtx.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/video_homography.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/videostab.cpp"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/samples/cpp/watershed.cpp"
    )
endif()

