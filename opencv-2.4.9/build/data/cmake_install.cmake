# Install script for directory: /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xlibsx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/OpenCV/haarcascades" TYPE FILE FILES
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_eye.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_eye_tree_eyeglasses.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_frontalface_alt.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_frontalface_alt2.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_frontalface_alt_tree.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_frontalface_default.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_fullbody.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_lefteye_2splits.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_lowerbody.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_mcs_eyepair_big.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_mcs_eyepair_small.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_mcs_leftear.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_mcs_lefteye.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_mcs_mouth.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_mcs_nose.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_mcs_rightear.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_mcs_righteye.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_mcs_upperbody.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_profileface.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_righteye_2splits.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_smile.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/haarcascades/haarcascade_upperbody.xml"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xlibsx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/OpenCV/lbpcascades" TYPE FILE FILES
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/lbpcascades/lbpcascade_frontalface.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/lbpcascades/lbpcascade_profileface.xml"
    "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/data/lbpcascades/lbpcascade_silverware.xml"
    )
endif()

