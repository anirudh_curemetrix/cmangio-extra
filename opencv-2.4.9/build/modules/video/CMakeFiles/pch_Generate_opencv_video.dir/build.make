# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.19

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Disable VCS-based implicit rules.
% : %,v


# Disable VCS-based implicit rules.
% : RCS/%


# Disable VCS-based implicit rules.
% : RCS/%,v


# Disable VCS-based implicit rules.
% : SCCS/s.%


# Disable VCS-based implicit rules.
% : s.%


.SUFFIXES: .hpux_make_needs_suffix_list


# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build

# Utility rule file for pch_Generate_opencv_video.

# Include the progress variables for this target.
include modules/video/CMakeFiles/pch_Generate_opencv_video.dir/progress.make

modules/video/CMakeFiles/pch_Generate_opencv_video: modules/video/precomp.hpp.gch/opencv_video_Release.gch


modules/video/precomp.hpp.gch/opencv_video_Release.gch: ../modules/video/src/precomp.hpp
modules/video/precomp.hpp.gch/opencv_video_Release.gch: modules/video/precomp.hpp
modules/video/precomp.hpp.gch/opencv_video_Release.gch: lib/libopencv_video_pch_dephelp.a
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Generating precomp.hpp.gch/opencv_video_Release.gch"
	cd /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/video && /usr/bin/cmake -E make_directory /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/video/precomp.hpp.gch
	cd /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/video && /usr/bin/c++ -O3 -DNDEBUG -DNDEBUG -fPIC -I"/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/imgproc/include" -I"/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/core/include" -isystem"/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/video" -I"/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/video/src" -I"/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/video/include" -isystem"/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build" -isystem"/usr/include/eigen3" -isystem"/usr/local/cuda-9.0/include" -fsigned-char -W -Wall -Werror=return-type -Werror=address -Werror=sequence-point -Wformat -Werror=format-security -Wmissing-declarations -Wundef -Winit-self -Wpointer-arith -Wshadow -Wsign-promo -Wno-narrowing -Wno-delete-non-virtual-dtor -fdiagnostics-show-option -Wno-long-long -pthread -fomit-frame-pointer -msse -msse2 -msse3 -ffunction-sections -DCVAPI_EXPORTS -x c++-header -o /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/video/precomp.hpp.gch/opencv_video_Release.gch /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/video/precomp.hpp

modules/video/precomp.hpp: ../modules/video/src/precomp.hpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Generating precomp.hpp"
	cd /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/video && /usr/bin/cmake -E copy /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/video/src/precomp.hpp /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/video/precomp.hpp

pch_Generate_opencv_video: modules/video/CMakeFiles/pch_Generate_opencv_video
pch_Generate_opencv_video: modules/video/precomp.hpp
pch_Generate_opencv_video: modules/video/precomp.hpp.gch/opencv_video_Release.gch
pch_Generate_opencv_video: modules/video/CMakeFiles/pch_Generate_opencv_video.dir/build.make

.PHONY : pch_Generate_opencv_video

# Rule to build all files generated by this target.
modules/video/CMakeFiles/pch_Generate_opencv_video.dir/build: pch_Generate_opencv_video

.PHONY : modules/video/CMakeFiles/pch_Generate_opencv_video.dir/build

modules/video/CMakeFiles/pch_Generate_opencv_video.dir/clean:
	cd /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/video && $(CMAKE_COMMAND) -P CMakeFiles/pch_Generate_opencv_video.dir/cmake_clean.cmake
.PHONY : modules/video/CMakeFiles/pch_Generate_opencv_video.dir/clean

modules/video/CMakeFiles/pch_Generate_opencv_video.dir/depend:
	cd /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9 /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/video /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/video /nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/video/CMakeFiles/pch_Generate_opencv_video.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : modules/video/CMakeFiles/pch_Generate_opencv_video.dir/depend

