# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.19

# compile CXX with /usr/bin/c++
CXX_DEFINES = -DUSE_CUDA

CXX_INCLUDES = -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/core/perf -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/features2d/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/highgui/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/imgproc/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/flann/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/core/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/ts/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/core/src -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/core/test -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/gpu/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/dynamicuda/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build -isystem /usr/include/eigen3 -isystem /usr/local/cuda-9.0/include

CXX_FLAGS =    -fsigned-char -W -Wall -Werror=return-type -Werror=address -Werror=sequence-point -Wformat -Werror=format-security -Wmissing-declarations  -Winit-self -Wpointer-arith  -Wsign-promo -Wno-narrowing -Wno-delete-non-virtual-dtor -fdiagnostics-show-option -Wno-long-long -pthread -fomit-frame-pointer -msse -msse2 -msse3 -ffunction-sections -Wno-undef -Wno-shadow -O3 -DNDEBUG  -DNDEBUG

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_abs.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_addWeighted.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_arithm.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_bitwise.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_compare.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_convertTo.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_dft.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_dot.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_inRange.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_main.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_mat.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_math.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_merge.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_minmaxloc.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_norm.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_reduce.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_split.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_perf_core.dir/perf/perf_stat.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/perf_precomp.hpp" -Winvalid-pch 

