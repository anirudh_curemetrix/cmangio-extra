# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.19

# compile CXX with /usr/bin/c++
CXX_DEFINES = -DCVAPI_EXPORTS -DUSE_CUDA

CXX_INCLUDES = -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/core/perf -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/features2d/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/highgui/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/imgproc/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/flann/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/core/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/ts/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/core/src -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/core/test -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/gpu/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/modules/dynamicuda/include -I/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build -isystem /usr/include/eigen3 -isystem /usr/local/cuda-9.0/include

CXX_FLAGS =    -fsigned-char -W -Wall -Werror=return-type -Werror=address -Werror=sequence-point -Wformat -Werror=format-security -Wmissing-declarations  -Winit-self -Wpointer-arith  -Wsign-promo -Wno-narrowing -Wno-delete-non-virtual-dtor -fdiagnostics-show-option -Wno-long-long -pthread -fomit-frame-pointer -msse -msse2 -msse3 -ffunction-sections -Wno-undef -Wno-shadow -O3 -DNDEBUG  -DNDEBUG -fPIC

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/algorithm.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/alloc.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/arithm.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/array.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/cmdparser.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/convert.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/copy.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/datastructs.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/drawing.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/dxt.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/gl_core_3_1.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/glob.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/gpumat.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/lapack.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/mathfuncs.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/matmul.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/matop.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/matrix.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/opengl_interop.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/opengl_interop_deprecated.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/out.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/parallel.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/persistence.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/rand.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/stat.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/system.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

# Custom flags: modules/core/CMakeFiles/opencv_core.dir/src/tables.cpp.o_FLAGS =  -include "/nfs/experiments/Anirudh_cmAngio_2020/opencv-2.4.9/build/modules/core/precomp.hpp" -Winvalid-pch 

